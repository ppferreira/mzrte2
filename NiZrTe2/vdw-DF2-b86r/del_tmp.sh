#!/bin/sh

for i in S*
do

cd $i

for j in NiZrTe2*
do

cd $j

rm -vfr tmp core*

cd ..

done

cd ..

done

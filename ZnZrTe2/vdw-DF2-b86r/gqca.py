# coding=utf-8
import csv
import numpy as np
import numpy.polynomial.polynomial as poly
import numpy.ma as ma
import scipy.constants as cst
import matplotlib.pyplot as plt
import matplotlib.ticker as mpt
from matplotlib import rc
from labellines import labelLine, labelLines
from colors import Palette
from scipy.spatial import ConvexHull

## Functions to calculate the average a, c and the van der waals gap parameter
def delta_x():
    delta_j = np.zeros(len(n_j))
    for j in range(len(n_j)):
        delta_j[j] = e_j[j] - ((n-n_j[j])/n)*e_j[0] - (n_j[j]/n)*e_j[21]
    return delta_j

def a_x():
    a_j = np.zeros(n+1)
    i=0
    for j in range(len(n_j)):
        if (n_j[j]==0) or (n_j[j]==n_j[j-1]):
            a_j[i] += (n*x - n_j[j])*g_j[j]*np.exp((delta_j[j]*-1)/(k_b*T))
        else:
            i += 1
            a_j[i] = (n*x - n_j[j])*g_j[j]*np.exp((delta_j[j]*-1)/(k_b*T))
    return a_j

def eta_x():
    eta = 0
    eta_i = np.roots(a_j[::-1])
    real = eta_i[np.isreal(eta_i)]
    for i in range(len(real)):
        if real[i]>0:
            eta = real[i]
    return eta

def x_x():
    x_j = np.zeros(len(n_j))
    for j in range(len(n_j)):
        sum=0
        for i in range(len(n_j)):
            sum += g_j[i]*(eta**n_j[i])*np.exp((delta_j[i]*-1)/(k_b*T))
        x_j[j] = (g_j[j]*(eta**n_j[j])*np.exp((delta_j[j]*-1)/(k_b*T)))/sum
    return x_j

def apar_x():
    apar_x=0
    squared_x=0
    for j in range(len(n_j)):
        apar_x += x_j[j]*apar_j[j]
        squared_x += x_j[j]*(apar_j[j]**2)
    stdev = np.sqrt(squared_x - (apar_x**2))
    return np.array([apar_x, stdev])

def vdw_x():
    vdw_x=0
    squared_x=0
    for j in range(len(n_j)):
        vdw_x += x_j[j]*vdw_j[j]
        squared_x += x_j[j]*(vdw_j[j]**2)
    stdev = np.sqrt(squared_x - (vdw_x**2))
    return np.array([vdw_x, stdev])

def c_x():
    c_x=0
    squared_x=0
    for j in range(len(n_j)):
        c_x += x_j[j]*c_j[j]
        squared_x += x_j[j]*(c_j[j]**2)
    stdev = np.sqrt(squared_x - (c_x**2))
    return np.array([c_x, stdev])

def vol_x():
    vol_x=0
    squared_x=0
    for j in range(len(n_j)):
        vol_x += x_j[j]*vol_j[j]
        squared_x += x_j[j]*(vol_j[j]**2)
    stdev = np.sqrt(squared_x - (vol_x**2))
    return np.array([vol_x, stdev])

def m_te_x():
    m_te_x=0
    squared_x=0
    for j in range(len(n_j)):
        m_te_x += x_j[j]*m_te_j[j]
        squared_x += x_j[j]*(m_te_j[j]**2)
    stdev = np.sqrt(squared_x - (m_te_x**2))
    return np.array([m_te_x, stdev])

def te_zr_x():
    te_zr_x=0
    squared_x=0
    for j in range(len(n_j)):
        te_zr_x += x_j[j]*te_zr_j[j]
        squared_x += x_j[j]*(te_zr_j[j]**2)
    stdev = np.sqrt(squared_x - (te_zr_x**2))
    return np.array([te_zr_x, stdev])

def m_zr_x():
    m_zr_x=0
    squared_x=0
    for j in range(len(n_j)):
        m_zr_x += x_j[j]*m_zr_j[j]
        squared_x += x_j[j]*(m_zr_j[j]**2)
    stdev = np.sqrt(squared_x - (m_zr_x**2))
    return np.array([m_zr_x, stdev])

def te_te_x():
    te_te_x=0
    squared_x=0
    for j in range(len(n_j)):
        te_te_x += x_j[j]*te_te_j[j]
        squared_x += x_j[j]*(te_te_j[j]**2)
    stdev = np.sqrt(squared_x - (te_te_x**2))
    return np.array([te_te_x, stdev])

def dos_x():
    dos_x=0
    squared_x=0
    for j in range(len(n_j)):
        dos_x += x_j[j]*dos_j[j]
        squared_x += x_j[j]*(dos_j[j]**2)
    stdev = np.sqrt(squared_x - (dos_x**2))
    return np.array([dos_x, stdev])

def pdos_x():
    pdos_x=np.zeros(37)
    squared_x=np.zeros(37)
    for j in range(len(n_j)):
        pdos_x += x_j[j]*pdos_j[j,:]
        squared_x += x_j[j]*(pdos_j[j]**2)
    stdev = np.sqrt(squared_x - (pdos_x**2))
    return np.array([pdos_x, stdev])

def delta_D_x():
    delta_D = 0
    for j in range(len(n_j)):
        x_0_j = g_j[j]*(x**n_j[j])*((1-x)**(n-n_j[j]))
        delta_D += x_j[j]*np.log((x_j[j]/x_0_j))
    return delta_D

def delta_U_x():
    delta_U = 0
    for j in range(len(n_j)):
        delta_U += x_j[j]*delta_j[j]
    return delta_U

def delta_S_x():
    delta_D = 0
    for j in range(len(n_j)):
        x_0_j = g_j[j]*(x**n_j[j])*((1-x)**(n-n_j[j]))
        delta_D += x_j[j]*np.log((x_j[j]/x_0_j))
    delta_S = (n*k_b*T*( (x*np.log(x)) + ((1-x)*np.log(1-x)) ) + k_b*T*delta_D)*-1
    return delta_S

def delta_F_x():
    delta_U = 0
    delta_D = 0
    for j in range(len(n_j)):
        x_0_j = g_j[j]*(x**n_j[j])*((1-x)**(n-n_j[j]))
        delta_U += x_j[j]*delta_j[j]
        delta_D += x_j[j]*np.log((x_j[j]/x_0_j))
    delta_F = delta_U + n*k_b*T*( (x*np.log(x)) + ((1-x)*np.log(1-x)) ) + k_b*T*delta_D
    return delta_F


def plot_latpar():
    fig, ax1 = plt.subplots(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    plt.ylim(3.7,4.3)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.yticks([])
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(3.92,4.04,step=0.02)))
    #plt.ylabel('a\,(\AA)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    ax1.yaxis.label.set_color('red')
    ax1.tick_params(axis='y', colors='red')
    line1, = plt.plot(x_i,apar[:,0], color='red', linestyle='-', linewidth='3', antialiased=True)
    line2, = plt.plot(x_m,apar_j, color='red', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:red', linestyle='none', antialiased=True)
    plt.fill_between(x_i, apar[:,0]-apar[:,1],apar[:,0]+apar[:,1], color='red', alpha=0.15)

    ax2 = ax1.twinx()

    plt.xlim(-0.05, 1.05)
    plt.ylim(6,7.6)
    plt.yticks([])
    #plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    #plt.ylabel('c\,(\AA)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    ax2.yaxis.label.set_color('blue')
    ax2.tick_params(axis='y', colors='blue')
    line1, = plt.plot(x_i,c[:,0], color='blue', linestyle='-', linewidth='3', antialiased=True)
    line2, = plt.plot(x_m,c_j, color='blue', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:blue', linestyle='none', antialiased=True)
    plt.fill_between(x_i, c[:,0]-c[:,1],c[:,0]+c[:,1], color='blue', alpha=0.15)
    plt.savefig('lat-par.pdf', bbox_inches='tight')


def plot_vdw():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('d$_{vdw}$\,(\AA)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,vdw[:,0], color='red', linestyle='-', linewidth='3', antialiased=True)
    line2, = plt.plot(x_m,vdw_j, color='red', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:red', linestyle='none', antialiased=True)
    plt.fill_between(x_i, vdw[:,0]-vdw[:,1],vdw[:,0]+vdw[:,1], color='red', alpha=0.15)
    plt.savefig('vdw.pdf', bbox_inches='tight')


def plot_vol():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('V\,(\AA)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,vol[:,0], color='red', linestyle='-', linewidth='3', antialiased=True)
    line2, = plt.plot(x_m,vol_j, color='red', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:red', linestyle='none', antialiased=True)
    plt.fill_between(x_i, vol[:,0]-vol[:,1],vol[:,0]+vol[:,1], color='red', alpha=0.15)
    plt.savefig('vol.pdf', bbox_inches='tight')

def plot_m_te():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('Average bond length\,(\AA)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,m_te[:,0], color='red', linestyle='-', linewidth='3', antialiased=True)
    line2, = plt.plot(x_m,m_te_j, color='red', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:red', linestyle='none', antialiased=True)
    plt.fill_between(x_i, m_te[:,0]-m_te[:,1],m_te[:,0]+m_te[:,1], color='red', alpha=0.15)
    plt.savefig('m_te.pdf', bbox_inches='tight')


def plot_te_zr():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('Average bond length\,(\AA)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,te_zr[:,0], color='red', linestyle='-', linewidth='3', antialiased=True)
    line2, = plt.plot(x_m,te_zr_j, color='red', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:red', linestyle='none', antialiased=True)
    plt.fill_between(x_i, te_zr[:,0]-te_zr[:,1],te_zr[:,0]+te_zr[:,1], color='red', alpha=0.15)
    plt.savefig('te_zr.pdf', bbox_inches='tight')

def plot_m_zr():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('Average bond length\,(\AA)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,m_zr[:,0], color='red', linestyle='-', linewidth='3', antialiased=True)
    line2, = plt.plot(x_m,m_zr_j, color='red', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:red', linestyle='none', antialiased=True)
    plt.fill_between(x_i, m_zr[:,0]-m_zr[:,1],m_zr[:,0]+m_zr[:,1], color='red', alpha=0.15)
    plt.savefig('m_zr.pdf', bbox_inches='tight')

def plot_te_te():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('Average bond length\,(\AA)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,te_te[:,0], color='red', linestyle='-', linewidth='3', antialiased=True)
    line2, = plt.plot(x_m,te_te_j, color='red', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:red', linestyle='none', antialiased=True)
    plt.fill_between(x_i, te_te[:,0]-te_te[:,1],te_te[:,0]+te_te[:,1], color='red', alpha=0.15)
    plt.savefig('te_te.pdf', bbox_inches='tight')

def plot_dos():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('DOS\,(states/eV/unit cell)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,dos[:,0], color='red', linestyle='-', linewidth='3', antialiased=True)
    line2, = plt.plot(x_m,dos_j, color='red', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:red', linestyle='none', antialiased=True)
    plt.fill_between(x_i, dos[:,0]-dos[:,1],dos[:,0]+dos[:,1], color='red', alpha=0.15)
    plt.savefig('dos.pdf', bbox_inches='tight')

def plot_pdos():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('DOS\,(states/eV/unit cell)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,pdos[:,-1], color='black', linestyle='-', linewidth='3', antialiased=True, label='Total')
    line2, = plt.plot(x_m,pdos_j[:,-1], color='black', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='grey', linestyle='none', antialiased=True)
    line3, = plt.plot(x_i,pdos[:,11], color='red', linestyle='-', linewidth='3', antialiased=True, label='Zr-d')
    line4, = plt.plot(x_m,pdos_j[:,11], color='red', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:red', linestyle='none', antialiased=True)
    line5, = plt.plot(x_i,pdos[:,17], color='blue', linestyle='-', linewidth='3', antialiased=True, label='Te-p')
    line6, = plt.plot(x_m,pdos_j[:,17], color='blue', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:blue', linestyle='none', antialiased=True)
    line7, = plt.plot(x_i,pdos[:,35], color='green', linestyle='-', linewidth='3', antialiased=True, label='Zn-d')
    line8, = plt.plot(x_m,pdos_j[:,35], color='green', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:green', linestyle='none', antialiased=True)
    plt.fill_between(x_i, pdos[:,-1] - stdev_pdos[:,-1], pdos[:,-1] + stdev_pdos[:,-1], color='black', alpha=0.15)
    plt.fill_between(x_i, pdos[:,11] - stdev_pdos[:,11], pdos[:,11] + stdev_pdos[:,11], color='red', alpha=0.15)
    plt.fill_between(x_i, pdos[:,17] - stdev_pdos[:,17], pdos[:,17] + stdev_pdos[:,17], color='blue', alpha=0.15)
    plt.fill_between(x_i, pdos[:,35] - stdev_pdos[:,35], pdos[:,35] + stdev_pdos[:,35], color='green', alpha=0.15)
    labelLines(plt.gca().get_lines(), align = False, xvals=(0.01,0.98), fontsize=14, zorder=2.5)
    plt.savefig('pdos.pdf', bbox_inches='tight')

def plot_x_j():
    fig2 = plt.figure(figsize=(5,5))
    #plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    #plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('x$_j$')
    plt.xlabel('T\,(K)')
    plt.plot(t_i,x_t, linestyle='-', linewidth='3', antialiased=True)
    plt.savefig('x_j.pdf', bbox_inches='tight')


def plot_excess():
    delta_j = delta_x()
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('$\Delta_j$\,(eV/cluster)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    line1, = plt.plot(x_Zn,delta_eV, color='red', marker='.', markeredgecolor='black', markeredgewidth=1.5, markersize=15, markerfacecolor='tab:red', linestyle='none', antialiased=True)
    for simplex in hull.simplices:
        line2, = plt.plot(enthalpy[simplex,0],enthalpy[simplex,1], color='red', linestyle='-', linewidth='3', antialiased=True)
    plt.savefig('excess_energy.pdf', bbox_inches='tight')


def plot_delta_U():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('$\Delta U$\,(eV/cluster)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,delta_U[0,:], color=colorplot[0], linestyle='-', linewidth='3', antialiased=True,label='100')
    line2, = plt.plot(x_i,delta_U[1,:], color=colorplot[10], linestyle='-', linewidth='3', antialiased=True,label='200')
    line3, = plt.plot(x_i,delta_U[2,:], color=colorplot[20], linestyle='-', linewidth='3', antialiased=True,label='300')
    line4, = plt.plot(x_i,delta_U[3,:], color=colorplot[30], linestyle='-', linewidth='3', antialiased=True,label='400')
    line5, = plt.plot(x_i,delta_U[4,:], color=colorplot[40], linestyle='-', linewidth='3', antialiased=True,label='500')
    line6, = plt.plot(x_i,delta_U[5,:], color=colorplot[50], linestyle='-', linewidth='3', antialiased=True,label='600')
    line7, = plt.plot(x_i,delta_U[6,:], color=colorplot[60], linestyle='-', linewidth='3', antialiased=True,label='700')
    line8, = plt.plot(x_i,delta_U[7,:], color=colorplot[70], linestyle='-', linewidth='3', antialiased=True,label='800')
    line9, = plt.plot(x_i,delta_U[8,:], color=colorplot[80], linestyle='-', linewidth='3', antialiased=True,label='900')
    line10, = plt.plot(x_i,delta_U[9,:], color=colorplot[90], linestyle='-', linewidth='3', antialiased=True,label='1000')
    line11, = plt.plot(x_i,delta_U[10,:], color=colorplot[100], linestyle='-', linewidth='3', antialiased=True,label='1100')
    line12, = plt.plot(x_i,delta_U[11,:], color=colorplot[110], linestyle='-', linewidth='3', antialiased=True,label='1200')
#    labelLines(plt.gca().get_lines(), align = False, xvals=(0.3,0.55), fontsize=12, zorder=2.5)
    plt.savefig('Delta_U.pdf', bbox_inches='tight')

def plot_delta_S():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('T$\Delta S$\,(eV/cluster)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,delta_S[0,:], color=colorplot[0], linestyle='-', linewidth='3', antialiased=True,label='100')
    line2, = plt.plot(x_i,delta_S[1,:], color=colorplot[10], linestyle='-', linewidth='3', antialiased=True,label='200')
    line3, = plt.plot(x_i,delta_S[2,:], color=colorplot[20], linestyle='-', linewidth='3', antialiased=True,label='300')
    line4, = plt.plot(x_i,delta_S[3,:], color=colorplot[30], linestyle='-', linewidth='3', antialiased=True,label='400')
    line5, = plt.plot(x_i,delta_S[4,:], color=colorplot[40], linestyle='-', linewidth='3', antialiased=True,label='500')
    line6, = plt.plot(x_i,delta_S[5,:], color=colorplot[50], linestyle='-', linewidth='3', antialiased=True,label='600')
    line7, = plt.plot(x_i,delta_S[6,:], color=colorplot[60], linestyle='-', linewidth='3', antialiased=True,label='700')
    line8, = plt.plot(x_i,delta_S[7,:], color=colorplot[70], linestyle='-', linewidth='3', antialiased=True,label='800')
    line9, = plt.plot(x_i,delta_S[8,:], color=colorplot[80], linestyle='-', linewidth='3', antialiased=True,label='900')
    line10, = plt.plot(x_i,delta_S[9,:], color=colorplot[90], linestyle='-', linewidth='3', antialiased=True,label='1000')
    line11, = plt.plot(x_i,delta_S[10,:], color=colorplot[100], linestyle='-', linewidth='3', antialiased=True,label='1100')
    line12, = plt.plot(x_i,delta_S[11,:], color=colorplot[110], linestyle='-', linewidth='3', antialiased=True,label='1200')
    labelLines(plt.gca().get_lines(), align = False, xvals=(0.3,0.55), fontsize=12, zorder=2.5)
    plt.savefig('Delta_S.pdf', bbox_inches='tight')

def plot_delta_F():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('$\Delta F$\,(eV/cluster)')
    plt.xlabel('$x$ in Zn$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,delta_F[0,:], color=colorplot[0], linestyle='-', linewidth='3', antialiased=True,label='100')
    line2, = plt.plot(x_i,delta_F[1,:], color=colorplot[10], linestyle='-', linewidth='3', antialiased=True,label='200')
    line3, = plt.plot(x_i,delta_F[2,:], color=colorplot[20], linestyle='-', linewidth='3', antialiased=True,label='300')
    line4, = plt.plot(x_i,delta_F[3,:], color=colorplot[30], linestyle='-', linewidth='3', antialiased=True,label='400')
    line5, = plt.plot(x_i,delta_F[4,:], color=colorplot[40], linestyle='-', linewidth='3', antialiased=True,label='500')
    line6, = plt.plot(x_i,delta_F[5,:], color=colorplot[50], linestyle='-', linewidth='3', antialiased=True,label='600')
    line7, = plt.plot(x_i,delta_F[6,:], color=colorplot[60], linestyle='-', linewidth='3', antialiased=True,label='700')
    line8, = plt.plot(x_i,delta_F[7,:], color=colorplot[70], linestyle='-', linewidth='3', antialiased=True,label='800')
    line9, = plt.plot(x_i,delta_F[8,:], color=colorplot[80], linestyle='-', linewidth='3', antialiased=True,label='900')
    line10, = plt.plot(x_i,delta_F[9,:], color=colorplot[90], linestyle='-', linewidth='3', antialiased=True,label='1000')
    line11, = plt.plot(x_i,delta_F[10,:], color=colorplot[100], linestyle='-', linewidth='3', antialiased=True,label='1100')
    line12, = plt.plot(x_i,delta_F[11,:], color=colorplot[110], linestyle='-', linewidth='3', antialiased=True,label='1200')
    labelLines(plt.gca().get_lines(), align = False, xvals=(0.3,0.55), fontsize=12, zorder=2.5)
    plt.savefig('Delta_F.pdf', bbox_inches='tight')

def plot_delta_D():
    fig2 = plt.figure(figsize=(5,5))
    #plt.xlim(-0.05, 1.05)
    #plt.ylim(3.96,4.05)
    plt.xticks((np.arange(0,400,step=50)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('$\Delta D$($x_j - x_j^0$)')
    plt.xlabel('T\,(K)')
    line1, = plt.plot(t_i,delta_D[0,:], color=colorplot[0], linestyle='-', linewidth='3', antialiased=True,label='x=0.125')
    line2, = plt.plot(t_i,delta_D[1,:], color=colorplot[20], linestyle='-', linewidth='3', antialiased=True,label='x=0.250')
    line3, = plt.plot(t_i,delta_D[2,:], color=colorplot[40], linestyle='-', linewidth='3', antialiased=True,label='x=0.375')
    line4, = plt.plot(t_i,delta_D[3,:], color=colorplot[60], linestyle='-', linewidth='3', antialiased=True,label='x=0.500')
    line5, = plt.plot(t_i,delta_D[4,:], color=colorplot[80], linestyle='-', linewidth='3', antialiased=True,label='x=0.625')
    line6, = plt.plot(t_i,delta_D[5,:], color=colorplot[100], linestyle='-', linewidth='3', antialiased=True,label='x=0.750')
    plt.legend(handles=[line1,line2,line3,line4, line5, line6], loc=1, fontsize='16')
    plt.savefig('Delta_D.pdf', bbox_inches='tight')


file = './gqca.dat'
data = np.genfromtxt(file)

file_2 = './dist.dat'
data_2 = np.genfromtxt(file_2)

k_b = cst.k/cst.e
n = 8
T = 1200
eV = 13.605662285137

n_j = np.int64(data[:,0]) ## Number of Zn atoms in the supercell
x_m = (data[:,1])
x_Zn = data[:,1]
g_j = data[:,2] ## Cluster degeneracy
e_j = (data[:,7])
apar_j = (data[:,4]/2)*cst.value('atomic unit of length')*10**10
c_j = data[:,5]*apar_j
#vdw_j = data[:,6]
vol_j = data[:,6]
#pdos_j = data_2[0:len(n_j),1:38]/8
m_te_j = data_2[:,4]
te_zr_j = data_2[:,5]
m_zr_j = data_2[:,6]
te_te_j = data_2[:,7]

x_i = np.linspace(0.0001,0.9999,num=2000)
t_i = np.linspace(10,800,num=1000)



rc('font',**{'family':'Palatino','serif':['Palatino'],'size':22})
rc('text', usetex=True)

colormap = Palette(120, 2, 'coolwarm')
colorplot = colormap.get_colors()


#Calculating the physical properties using GQCA
#vdw = np.zeros((len(x_i),2))
apar = np.zeros((len(x_i),2))
c = np.zeros((len(x_i),2))
vol = np.zeros((len(x_i),2))
m_te = np.zeros((len(x_i),2))
te_zr = np.zeros((len(x_i),2))
m_zr = np.zeros((len(x_i),2))
te_te = np.zeros((len(x_i),2))
#dos = np.zeros((len(x_i),2))
#pdos = np.zeros((len(x_i),37))
#stdev_pdos = np.zeros((len(x_i),37))
delta_j = delta_x()
delta_eV = delta_j*eV
enthalpy = np.column_stack((x_m,delta_eV))
enthalpy = np.append(enthalpy, [[1,0]], axis=0)
enthalpy = np.insert(enthalpy,0,[[0,0]], axis=0)
hull = ConvexHull(enthalpy)

for k in range(len(x_i)):
    x = x_i[k]
    print('Calculating the coefficients a_j for x = {} and T={}...'.format(x,T))
    a_j = a_x()
    print('Done!')
    print('Calculating the eta roots of the {}-polynomial equation for x = {} and T={}...'.format(n,x,T))
    eta = eta_x()
    print('Done!')
    print('Calculating the probabilities x_j for x = {} and T={}...'.format(x,T))
    x_j = x_x()
    print('Done!')
    print('Calculating the lattice parameter a for x = {} and T={}...'.format(x,T))
    apar[k] = apar_x()
    print('Done!')
    print('Calculating the lattice parameter c for x = {} and T={}...'.format(x,T))
    c[k] = c_x()
    print('Done!')
#    print('Calculating the van der Waals gap c for x = {} and T={}...'.format(x,T))
#    vdw[k] = vdw_x()
#    print('Done!')
    print('Calculating the lattice parameter c for x = {} and T={}...'.format(x,T))
    vol[k] = vol_x()
    print('Done!')
    print('Calculating the M--Te bond length for x = {} and T={}...'.format(x,T))
    m_te[k] = m_te_x()
    print('Done!')
    print('Calculating the Te--Zr bond length for x = {} and T={}...'.format(x,T))
    te_zr[k] = te_zr_x()
    print('Done!')
    print('Calculating the M--Zr bond length for x = {} and T={}...'.format(x,T))
    m_zr[k] = m_zr_x()
    print('Done!')
    print('Calculating the Te--Te bond length for x = {} and T={}...'.format(x,T))
    te_te[k] = te_te_x()
    print('Done!')
#    print('Calculating densities of states for x = {} and T={}...'.format(x,T))
#    pdos[k], stdev_pdos[k] = pdos_x()
#    print('Done!')

#pdos_total=pdos[:,1]+pdos[:,5]+pdos[:,11]+pdos[:,1]+pdos[:,13]+pdos[:,17]+pdos[:,23]+pdos[:,25]+pdos[:,29]+pdos[:,35]

print('Plotting the lattice parameters...')
plot_latpar()
print('Plotting the volume...')
plot_vol()
print('Plotting the M--Te bond length...')
plot_m_te()
print('Plotting the Te--Zr bond length...')
plot_te_zr()
print('Plotting the M--Zr bond length...')
plot_m_zr()
print('Plotting the Te--Te bond length...')
plot_te_te()
print('Plotting the excess free energies...')
plot_excess()


x = 0.5
x_t = np.zeros((len(t_i),22))
delta_D = np.zeros(len(t_i))
print('Calculating the cluster fraction probabilities x_j for x = {} in function of temperature...'.format(x))
for t in range(len(t_i)):
    T = t_i[t]
    a_j = a_x()
    eta = eta_x()
    x_t[t] = x_x()
print('Plotting the cluster fraction probabilities x_j for x = {} in function of temperature...'.format(x))
plot_x_j()

t = 0
delta_F = np.zeros((12,len(x_i)))
delta_U = np.zeros((12,len(x_i)))
delta_S = np.zeros((12,len(x_i)))
for T in [100,200,300,400,500,600,700,800,900,1000,1100,1200]:
    delta_F_t = np.zeros((len(x_i)))
    delta_U_t = np.zeros((len(x_i)))
    delta_S_t = np.zeros((len(x_i)))
    print('Calculating the mixing free energy for T = {}...'.format(T))
    for k in range(len(x_i)):
        x = x_i[k]
        a_j = a_x()
        eta = eta_x()
        x_j = x_x()
        delta_U_t[k] = delta_U_x()
        delta_S_t[k] = delta_S_x()
        delta_F_t[k] = delta_F_x()
    delta_U[t] = delta_U_t*eV
    delta_S[t] = delta_S_t*eV
    delta_F[t] = delta_F_t*eV
    print('Done!')
    t += 1

print('Plotting the mixing internal energies...')
plot_delta_U()
print('Plotting the mixing entropies...')
plot_delta_S()
print('Plotting the mixing free energies...')
plot_delta_F()


t = 0
t_i = np.linspace(10,350,num=1000)
delta_D = np.zeros((6,len(t_i)))
for x in [0.125, 0.25, 0.375, 0.50, 0.625, 0.75]:
    print('Calculating the Kullback-Leibler divergence for x = {}...'.format(x))
    delta_D_t = np.zeros((len(t_i)))
    for k in range(len(t_i)):
        T = t_i[k]
        a_j = a_x()
        eta = eta_x()
        x_j = x_x()
        delta_D_t[k] = delta_D_x()
    delta_D[t] = delta_D_t
    print('Done!')
    t += 1

print('Plotting the Kullback-Leibler divergence in function of temperature...'.format(x))
plot_delta_D()
print('Succesfully completed!! =)')
# #coef = eta()
# ## Numerical parameters

# coding=utf-8
import csv
import numpy as np
import numpy.polynomial.polynomial as poly
import numpy.ma as ma
import scipy.constants as cst
import matplotlib.pyplot as plt
import matplotlib.ticker as mpt
from matplotlib import rc
from labellines import labelLine, labelLines
from colors import Palette
from scipy.spatial import ConvexHull

## Functions to calculate the average a, c and the van der waals gap parameter
def delta_x():
    delta_j = np.zeros(len(n_j))
    for j in range(len(n_j)):
        delta_j[j] = e_j[j] - ((n-n_j[j])/n)*e_j[0] - (n_j[j]/n)*e_j[21]
    return delta_j

def a_x():
    a_j = np.zeros(n+1)
    i=0
    for j in range(len(n_j)):
        if (n_j[j]==0) or (n_j[j]==n_j[j-1]):
            a_j[i] += (n*x - n_j[j])*g_j[j]*np.exp((delta_j[j]*-1)/(k_b*T))
        else:
            i += 1
            a_j[i] = (n*x - n_j[j])*g_j[j]*np.exp((delta_j[j]*-1)/(k_b*T))
    return a_j

def eta_x():
    eta = 0
    eta_i = np.roots(a_j[::-1])
    real = eta_i[np.isreal(eta_i)]
    for i in range(len(real)):
        if real[i]>0:
            eta = real[i]
    return eta

def x_x():
    x_j = np.zeros(len(n_j))
    for j in range(len(n_j)):
        sum=0
        for i in range(len(n_j)):
            sum += g_j[i]*(eta**n_j[i])*np.exp((delta_j[i]*-1)/(k_b*T))
        x_j[j] = (g_j[j]*(eta**n_j[j])*np.exp((delta_j[j]*-1)/(k_b*T)))/sum
    return x_j

def m_te_x():
    m_te_x=0
    squared_x=0
    for j in range(len(n_j)):
        m_te_x += x_j[j]*m_te_j[j]
        squared_x += x_j[j]*(m_te_j[j]**2)
    stdev = np.sqrt(squared_x - (m_te_x**2))
    return np.array([m_te_x, stdev])

def te_zr_x():
    te_zr_x=0
    squared_x=0
    for j in range(len(n_j)):
        te_zr_x += x_j[j]*te_zr_j[j]
        squared_x += x_j[j]*(te_zr_j[j]**2)
    stdev = np.sqrt(squared_x - (te_zr_x**2))
    return np.array([te_zr_x, stdev])

def m_zr_x():
    m_zr_x=0
    squared_x=0
    for j in range(len(n_j)):
        m_zr_x += x_j[j]*m_zr_j[j]
        squared_x += x_j[j]*(m_zr_j[j]**2)
    stdev = np.sqrt(squared_x - (m_zr_x**2))
    return np.array([m_zr_x, stdev])

def te_te_x():
    te_te_x=0
    squared_x=0
    for j in range(len(n_j)):
        te_te_x += x_j[j]*te_te_j[j]
        squared_x += x_j[j]*(te_te_j[j]**2)
    stdev = np.sqrt(squared_x - (te_te_x**2))
    return np.array([te_te_x, stdev])

def delta_D_x():
    delta_D = 0
    for j in range(len(n_j)):
        x_0_j = g_j[j]*(x**n_j[j])*((1-x)**(n-n_j[j]))
        delta_D += x_j[j]*np.log((x_j[j]/x_0_j))
    return delta_D

def delta_U_x():
    delta_U = 0
    for j in range(len(n_j)):
        delta_U += x_j[j]*delta_j[j]
    return delta_U

def delta_S_x():
    delta_D = 0
    for j in range(len(n_j)):
        x_0_j = g_j[j]*(x**n_j[j])*((1-x)**(n-n_j[j]))
        delta_D += x_j[j]*np.log((x_j[j]/x_0_j))
    delta_S = (n*k_b*T*( (x*np.log(x)) + ((1-x)*np.log(1-x)) ) + k_b*T*delta_D)*-1
    return delta_S

def delta_F_x():
    delta_U = 0
    delta_D = 0
    for j in range(len(n_j)):
        x_0_j = g_j[j]*(x**n_j[j])*((1-x)**(n-n_j[j]))
        delta_U += x_j[j]*delta_j[j]
        delta_D += x_j[j]*np.log((x_j[j]/x_0_j))
    delta_F = delta_U + n*k_b*T*( (x*np.log(x)) + ((1-x)*np.log(1-x)) ) + k_b*T*delta_D
    return delta_F

def plot_m_te():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(.125, 1.05)
    plt.ylim(2,3)
    plt.xticks((np.arange(0.25,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('Average bond length\,(\AA)')
    plt.xlabel('$x$ in M$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,m_te_Sc[:,0], color=colorplot[0], linestyle='-', linewidth='3', antialiased=True,label='Sc')
    line2, = plt.plot(x_i,m_te_Ti[:,0], color=colorplot[10], linestyle='-', linewidth='3', antialiased=True,label='Ti')
    line3, = plt.plot(x_i,m_te_V[:,0], color=colorplot[20], linestyle='-', linewidth='3', antialiased=True,label='V')
    line4, = plt.plot(x_i,m_te_Cr[:,0], color=colorplot[30], linestyle='-', linewidth='3', antialiased=True,label='Cr')
    line5, = plt.plot(x_i,m_te_Fe[:,0], color=colorplot[40], linestyle='-', linewidth='3', antialiased=True,label='Fe')
    line6, = plt.plot(x_i,m_te_Co[:,0], color=colorplot[50], linestyle='-', linewidth='3', antialiased=True,label='Co')
    line7, = plt.plot(x_i,m_te_Ni[:,0], color=colorplot[60], linestyle='-', linewidth='3', antialiased=True,label='Ni')
    line8, = plt.plot(x_i,m_te_Cu[:,0], color=colorplot[70], linestyle='-', linewidth='3', antialiased=True,label='Cu')
    line9, = plt.plot(x_i,m_te_Zn[:,0], color=colorplot[80], linestyle='-', linewidth='3', antialiased=True,label='Zn')
    labelLines(plt.gca().get_lines(), align = False, xvals=(0.05,0.95), fontsize=14, zorder=2.5)
    plt.savefig('m_te.pdf', bbox_inches='tight')

def plot_te_zr():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
    #plt.ylim(2.5,3)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('Average bond length\,(\AA)')
    plt.xlabel('$x$ in M$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,te_zr_Sc[:,0], color=colorplot[0], linestyle='-', linewidth='3', antialiased=True,label='Sc')
    line2, = plt.plot(x_i,te_zr_Ti[:,0], color=colorplot[10], linestyle='-', linewidth='3', antialiased=True,label='Ti')
    line3, = plt.plot(x_i,te_zr_V[:,0], color=colorplot[20], linestyle='-', linewidth='3', antialiased=True,label='V')
    line4, = plt.plot(x_i,te_zr_Cr[:,0], color=colorplot[30], linestyle='-', linewidth='3', antialiased=True,label='Cr')
    line5, = plt.plot(x_i,te_zr_Fe[:,0], color=colorplot[40], linestyle='-', linewidth='3', antialiased=True,label='Fe')
    line6, = plt.plot(x_i,te_zr_Co[:,0], color=colorplot[50], linestyle='-', linewidth='3', antialiased=True,label='Co')
    line7, = plt.plot(x_i,te_zr_Ni[:,0], color=colorplot[60], linestyle='-', linewidth='3', antialiased=True,label='Ni')
    line8, = plt.plot(x_i,te_zr_Cu[:,0], color=colorplot[70], linestyle='-', linewidth='3', antialiased=True,label='Cu')
    line9, = plt.plot(x_i,te_zr_Zn[:,0], color=colorplot[80], linestyle='-', linewidth='3', antialiased=True,label='Zn')
    labelLines(plt.gca().get_lines(), align = False, xvals=(0.05,0.95), fontsize=14, zorder=2.5)
    plt.savefig('te_zr.pdf', bbox_inches='tight')

def plot_m_zr():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(.125, 1.05)
    plt.ylim(2,3)
    plt.xticks((np.arange(0.25,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('Average bond length\,(\AA)')
    plt.xlabel('$x$ in M$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,m_zr_Sc[:,0], color=colorplot[0], linestyle='-', linewidth='3', antialiased=True,label='Sc')
    line2, = plt.plot(x_i,m_zr_Ti[:,0], color=colorplot[10], linestyle='-', linewidth='3', antialiased=True,label='Ti')
    line3, = plt.plot(x_i,m_zr_V[:,0], color=colorplot[20], linestyle='-', linewidth='3', antialiased=True,label='V')
    line4, = plt.plot(x_i,m_zr_Cr[:,0], color=colorplot[30], linestyle='-', linewidth='3', antialiased=True,label='Cr')
    line5, = plt.plot(x_i,m_zr_Fe[:,0], color=colorplot[40], linestyle='-', linewidth='3', antialiased=True,label='Fe')
    line6, = plt.plot(x_i,m_zr_Co[:,0], color=colorplot[50], linestyle='-', linewidth='3', antialiased=True,label='Co')
    line7, = plt.plot(x_i,m_zr_Ni[:,0], color=colorplot[60], linestyle='-', linewidth='3', antialiased=True,label='Ni')
    line8, = plt.plot(x_i,m_zr_Cu[:,0], color=colorplot[70], linestyle='-', linewidth='3', antialiased=True,label='Cu')
    line9, = plt.plot(x_i,m_zr_Zn[:,0], color=colorplot[80], linestyle='-', linewidth='3', antialiased=True,label='Zn')
    labelLines(plt.gca().get_lines(), align = False, xvals=(0.05,0.95), fontsize=14, zorder=2.5)
    plt.savefig('m_zr.pdf', bbox_inches='tight')

def plot_te_te():
    fig2 = plt.figure(figsize=(5,5))
    plt.xlim(-0.05, 1.05)
   # plt.ylim(2.5,3)
    plt.xticks((np.arange(0,1.25,step=0.25)))
    plt.tick_params(axis='both',direction='in',bottom=True, top=True, left=True, right=True)
    #plt.text(0.28, 4.02, 'vdW-DF2-b86r', horizontalalignment='center', verticalalignment='center')
    #plt.yticks((np.arange(0,2.1,step=0.5)))
    plt.ylabel('Average bond length\,(\AA)')
    plt.xlabel('$x$ in M$_x$ZrTe$_2$')
    line1, = plt.plot(x_i,te_te_Sc[:,0], color=colorplot[0], linestyle='-', linewidth='3', antialiased=True,label='Sc')
    line2, = plt.plot(x_i,te_te_Ti[:,0], color=colorplot[10], linestyle='-', linewidth='3', antialiased=True,label='Ti')
    line3, = plt.plot(x_i,te_te_V[:,0], color=colorplot[20], linestyle='-', linewidth='3', antialiased=True,label='V')
    line4, = plt.plot(x_i,te_te_Cr[:,0], color=colorplot[30], linestyle='-', linewidth='3', antialiased=True,label='Cr')
    line5, = plt.plot(x_i,te_te_Fe[:,0], color=colorplot[40], linestyle='-', linewidth='3', antialiased=True,label='Fe')
    line6, = plt.plot(x_i,te_te_Co[:,0], color=colorplot[50], linestyle='-', linewidth='3', antialiased=True,label='Co')
    line7, = plt.plot(x_i,te_te_Ni[:,0], color=colorplot[60], linestyle='-', linewidth='3', antialiased=True,label='Ni')
    line8, = plt.plot(x_i,te_te_Cu[:,0], color=colorplot[70], linestyle='-', linewidth='3', antialiased=True,label='Cu')
    line9, = plt.plot(x_i,te_te_Zn[:,0], color=colorplot[80], linestyle='-', linewidth='3', antialiased=True,label='Zn')
    labelLines(plt.gca().get_lines(), align = False, xvals=(0.05,0.95), fontsize=14, zorder=2.5)
    plt.savefig('te_te.pdf', bbox_inches='tight')

k_b = cst.k/cst.e
n = 8
T = 1200
eV = 13.605662285137

###########################################################################################################################

file = './gqca_Co.dat'
data = np.genfromtxt(file)

file_2 = './dist_Co.dat'
data_2 = np.genfromtxt(file_2)

n_j = np.int64(data[:,0]) ## Number of M atoms in the supercell
x_m = (data[:,1])
x_Zn = data[:,1]
g_j = data[:,2] ## Cluster degeneracy
e_j = (data[:,7])
m_te_j = data_2[:,4]
te_zr_j = data_2[:,5]
m_zr_j = data_2[:,6]
te_te_j = data_2[:,7]

x_i = np.linspace(0.0001,0.9999,num=2000)
t_i = np.linspace(10,800,num=1000)

#Calculating the physical properties using GQCA
m_te_Co = np.zeros((len(x_i),2))
te_zr_Co = np.zeros((len(x_i),2))
m_zr_Co = np.zeros((len(x_i),2))
te_te_Co = np.zeros((len(x_i),2))
delta_j = delta_x()
delta_eV = delta_j*eV
enthalpy = np.column_stack((x_m,delta_eV))
enthalpy = np.append(enthalpy, [[1,0]], axis=0)
enthalpy = np.insert(enthalpy,0,[[0,0]], axis=0)
hull = ConvexHull(enthalpy)

for k in range(len(x_i)):
    x = x_i[k]
    print('Calculating the coefficients a_j for x = {} and T={}...'.format(x,T))
    a_j = a_x()
    print('Done!')
    print('Calculating the eta roots of the {}-polynomial equation for x = {} and T={}...'.format(n,x,T))
    eta = eta_x()
    print('Done!')
    print('Calculating the probabilities x_j for x = {} and T={}...'.format(x,T))
    x_j = x_x()
    print('Done!')
    print('Calculating the Co--Te bond length for x = {} and T={}...'.format(x,T))
    m_te_Co[k] = m_te_x()
    print('Done!')
    print('Calculating the Te--Zr bond length for x = {} and T={}...'.format(x,T))
    te_zr_Co[k] = te_zr_x()
    print('Done!')
    print('Calculating the Co--Zr bond length for x = {} and T={}...'.format(x,T))
    m_zr_Co[k] = m_zr_x()
    print('Done!')
    print('Calculating the Te--Te bond length for x = {} and T={}...'.format(x,T))
    te_te_Co[k] = te_te_x()
    print('Done!')

###############################################################################################################

file = './gqca_Cr.dat'
data = np.genfromtxt(file)

file_2 = './dist_Cr.dat'
data_2 = np.genfromtxt(file_2)

n_j = np.int64(data[:,0]) ## Number of M atoms in the supercell
x_m = (data[:,1])
x_Zn = data[:,1]
g_j = data[:,2] ## Cluster degeneracy
e_j = (data[:,7])
m_te_j = data_2[:,4]
te_zr_j = data_2[:,5]
m_zr_j = data_2[:,6]
te_te_j = data_2[:,7]

x_i = np.linspace(0.0001,0.9999,num=2000)
t_i = np.linspace(10,800,num=1000)

#Calculating the physical properties using GQCA
m_te_Cr = np.zeros((len(x_i),2))
te_zr_Cr = np.zeros((len(x_i),2))
m_zr_Cr = np.zeros((len(x_i),2))
te_te_Cr = np.zeros((len(x_i),2))
delta_j = delta_x()
delta_eV = delta_j*eV
enthalpy = np.column_stack((x_m,delta_eV))
enthalpy = np.append(enthalpy, [[1,0]], axis=0)
enthalpy = np.insert(enthalpy,0,[[0,0]], axis=0)
hull = ConvexHull(enthalpy)

for k in range(len(x_i)):
    x = x_i[k]
    print('Calculating the coefficients a_j for x = {} and T={}...'.format(x,T))
    a_j = a_x()
    print('Done!')
    print('Calculating the eta roots of the {}-polynomial equation for x = {} and T={}...'.format(n,x,T))
    eta = eta_x()
    print('Done!')
    print('Calculating the probabilities x_j for x = {} and T={}...'.format(x,T))
    x_j = x_x()
    print('Done!')
    print('Calculating the Cr--Te bond length for x = {} and T={}...'.format(x,T))
    m_te_Cr[k] = m_te_x()
    print('Done!')
    print('Calculating the Te--Zr bond length for x = {} and T={}...'.format(x,T))
    te_zr_Cr[k] = te_zr_x()
    print('Done!')
    print('Calculating the Cr--Zr bond length for x = {} and T={}...'.format(x,T))
    m_zr_Cr[k] = m_zr_x()
    print('Done!')
    print('Calculating the Te--Te bond length for x = {} and T={}...'.format(x,T))
    te_te_Cr[k] = te_te_x()
    print('Done!')

###############################################################################################################

file = './gqca_Cu.dat'
data = np.genfromtxt(file)

file_2 = './dist_Cu.dat'
data_2 = np.genfromtxt(file_2)

n_j = np.int64(data[:,0]) ## Number of M atoms in the supercell
x_m = (data[:,1])
x_Zn = data[:,1]
g_j = data[:,2] ## Cluster degeneracy
e_j = (data[:,7])
m_te_j = data_2[:,4]
te_zr_j = data_2[:,5]
m_zr_j = data_2[:,6]
te_te_j = data_2[:,7]

x_i = np.linspace(0.0001,0.9999,num=2000)
t_i = np.linspace(10,800,num=1000)

#Calculating the physical properties using GQCA
m_te_Cu = np.zeros((len(x_i),2))
te_zr_Cu = np.zeros((len(x_i),2))
m_zr_Cu = np.zeros((len(x_i),2))
te_te_Cu = np.zeros((len(x_i),2))
delta_j = delta_x()
delta_eV = delta_j*eV
enthalpy = np.column_stack((x_m,delta_eV))
enthalpy = np.append(enthalpy, [[1,0]], axis=0)
enthalpy = np.insert(enthalpy,0,[[0,0]], axis=0)
hull = ConvexHull(enthalpy)

for k in range(len(x_i)):
    x = x_i[k]
    print('Calculating the coefficients a_j for x = {} and T={}...'.format(x,T))
    a_j = a_x()
    print('Done!')
    print('Calculating the eta roots of the {}-polynomial equation for x = {} and T={}...'.format(n,x,T))
    eta = eta_x()
    print('Done!')
    print('Calculating the probabilities x_j for x = {} and T={}...'.format(x,T))
    x_j = x_x()
    print('Done!')
    print('Calculating the Cu--Te bond length for x = {} and T={}...'.format(x,T))
    m_te_Cu[k] = m_te_x()
    print('Done!')
    print('Calculating the Te--Zr bond length for x = {} and T={}...'.format(x,T))
    te_zr_Cu[k] = te_zr_x()
    print('Done!')
    print('Calculating the Cu--Zr bond length for x = {} and T={}...'.format(x,T))
    m_zr_Cu[k] = m_zr_x()
    print('Done!')
    print('Calculating the Te--Te bond length for x = {} and T={}...'.format(x,T))
    te_te_Cu[k] = te_te_x()
    print('Done!')

###############################################################################################################

file = './gqca_Fe.dat'
data = np.genfromtxt(file)

file_2 = './dist_Fe.dat'
data_2 = np.genfromtxt(file_2)

n_j = np.int64(data[:,0]) ## Number of M atoms in the supercell
x_m = (data[:,1])
x_Zn = data[:,1]
g_j = data[:,2] ## Cluster degeneracy
e_j = (data[:,7])
m_te_j = data_2[:,4]
te_zr_j = data_2[:,5]
m_zr_j = data_2[:,6]
te_te_j = data_2[:,7]

x_i = np.linspace(0.0001,0.9999,num=2000)
t_i = np.linspace(10,800,num=1000)

#Calculating the physical properties using GQCA
m_te_Fe = np.zeros((len(x_i),2))
te_zr_Fe = np.zeros((len(x_i),2))
m_zr_Fe = np.zeros((len(x_i),2))
te_te_Fe = np.zeros((len(x_i),2))
delta_j = delta_x()
delta_eV = delta_j*eV
enthalpy = np.column_stack((x_m,delta_eV))
enthalpy = np.append(enthalpy, [[1,0]], axis=0)
enthalpy = np.insert(enthalpy,0,[[0,0]], axis=0)
hull = ConvexHull(enthalpy)

for k in range(len(x_i)):
    x = x_i[k]
    print('Calculating the coefficients a_j for x = {} and T={}...'.format(x,T))
    a_j = a_x()
    print('Done!')
    print('Calculating the eta roots of the {}-polynomial equation for x = {} and T={}...'.format(n,x,T))
    eta = eta_x()
    print('Done!')
    print('Calculating the probabilities x_j for x = {} and T={}...'.format(x,T))
    x_j = x_x()
    print('Done!')
    print('Calculating the Fe--Te bond length for x = {} and T={}...'.format(x,T))
    m_te_Fe[k] = m_te_x()
    print('Done!')
    print('Calculating the Te--Zr bond length for x = {} and T={}...'.format(x,T))
    te_zr_Fe[k] = te_zr_x()
    print('Done!')
    print('Calculating the Fe--Zr bond length for x = {} and T={}...'.format(x,T))
    m_zr_Fe[k] = m_zr_x()
    print('Done!')
    print('Calculating the Te--Te bond length for x = {} and T={}...'.format(x,T))
    te_te_Fe[k] = te_te_x()
    print('Done!')

###############################################################################################################

file = './gqca_Ni.dat'
data = np.genfromtxt(file)

file_2 = './dist_Ni.dat'
data_2 = np.genfromtxt(file_2)

n_j = np.int64(data[:,0]) ## Number of M atoms in the supercell
x_m = (data[:,1])
x_Zn = data[:,1]
g_j = data[:,2] ## Cluster degeneracy
e_j = (data[:,7])
m_te_j = data_2[:,4]
te_zr_j = data_2[:,5]
m_zr_j = data_2[:,6]
te_te_j = data_2[:,7]

x_i = np.linspace(0.0001,0.9999,num=2000)
t_i = np.linspace(10,800,num=1000)

#Calculating the physical properties using GQCA
m_te_Ni = np.zeros((len(x_i),2))
te_zr_Ni = np.zeros((len(x_i),2))
m_zr_Ni = np.zeros((len(x_i),2))
te_te_Ni = np.zeros((len(x_i),2))
delta_j = delta_x()
delta_eV = delta_j*eV
enthalpy = np.column_stack((x_m,delta_eV))
enthalpy = np.append(enthalpy, [[1,0]], axis=0)
enthalpy = np.insert(enthalpy,0,[[0,0]], axis=0)
hull = ConvexHull(enthalpy)

for k in range(len(x_i)):
    x = x_i[k]
    print('Calculating the coefficients a_j for x = {} and T={}...'.format(x,T))
    a_j = a_x()
    print('Done!')
    print('Calculating the eta roots of the {}-polynomial equation for x = {} and T={}...'.format(n,x,T))
    eta = eta_x()
    print('Done!')
    print('Calculating the probabilities x_j for x = {} and T={}...'.format(x,T))
    x_j = x_x()
    print('Done!')
    print('Calculating the Ni--Te bond length for x = {} and T={}...'.format(x,T))
    m_te_Ni[k] = m_te_x()
    print('Done!')
    print('Calculating the Te--Zr bond length for x = {} and T={}...'.format(x,T))
    te_zr_Ni[k] = te_zr_x()
    print('Done!')
    print('Calculating the Ni--Zr bond length for x = {} and T={}...'.format(x,T))
    m_zr_Ni[k] = m_zr_x()
    print('Done!')
    print('Calculating the Te--Te bond length for x = {} and T={}...'.format(x,T))
    te_te_Ni[k] = te_te_x()
    print('Done!')

###############################################################################################################

file = './gqca_Sc.dat'
data = np.genfromtxt(file)

file_2 = './dist_Sc.dat'
data_2 = np.genfromtxt(file_2)

n_j = np.int64(data[:,0]) ## Number of M atoms in the supercell
x_m = (data[:,1])
x_Zn = data[:,1]
g_j = data[:,2] ## Cluster degeneracy
e_j = (data[:,7])
m_te_j = data_2[:,4]
te_zr_j = data_2[:,5]
m_zr_j = data_2[:,6]
te_te_j = data_2[:,7]

x_i = np.linspace(0.0001,0.9999,num=2000)
t_i = np.linspace(10,800,num=1000)

#Calculating the physical properties using GQCA
m_te_Sc = np.zeros((len(x_i),2))
te_zr_Sc = np.zeros((len(x_i),2))
m_zr_Sc = np.zeros((len(x_i),2))
te_te_Sc = np.zeros((len(x_i),2))
delta_j = delta_x()
delta_eV = delta_j*eV
enthalpy = np.column_stack((x_m,delta_eV))
enthalpy = np.append(enthalpy, [[1,0]], axis=0)
enthalpy = np.insert(enthalpy,0,[[0,0]], axis=0)
hull = ConvexHull(enthalpy)

for k in range(len(x_i)):
    x = x_i[k]
    print('Calculating the coefficients a_j for x = {} and T={}...'.format(x,T))
    a_j = a_x()
    print('Done!')
    print('Calculating the eta roots of the {}-polynomial equation for x = {} and T={}...'.format(n,x,T))
    eta = eta_x()
    print('Done!')
    print('Calculating the probabilities x_j for x = {} and T={}...'.format(x,T))
    x_j = x_x()
    print('Done!')
    print('Calculating the Sc--Te bond length for x = {} and T={}...'.format(x,T))
    m_te_Sc[k] = m_te_x()
    print('Done!')
    print('Calculating the Te--Zr bond length for x = {} and T={}...'.format(x,T))
    te_zr_Sc[k] = te_zr_x()
    print('Done!')
    print('Calculating the Sc--Zr bond length for x = {} and T={}...'.format(x,T))
    m_zr_Sc[k] = m_zr_x()
    print('Done!')
    print('Calculating the Te--Te bond length for x = {} and T={}...'.format(x,T))
    te_te_Sc[k] = te_te_x()
    print('Done!')

###############################################################################################################

file = './gqca_Ti.dat'
data = np.genfromtxt(file)

file_2 = './dist_Ti.dat'
data_2 = np.genfromtxt(file_2)

n_j = np.int64(data[:,0]) ## Number of M atoms in the supercell
x_m = (data[:,1])
x_Zn = data[:,1]
g_j = data[:,2] ## Cluster degeneracy
e_j = (data[:,7])
m_te_j = data_2[:,4]
te_zr_j = data_2[:,5]
m_zr_j = data_2[:,6]
te_te_j = data_2[:,7]

x_i = np.linspace(0.0001,0.9999,num=2000)
t_i = np.linspace(10,800,num=1000)

#Calculating the physical properties using GQCA
m_te_Ti = np.zeros((len(x_i),2))
te_zr_Ti = np.zeros((len(x_i),2))
m_zr_Ti = np.zeros((len(x_i),2))
te_te_Ti = np.zeros((len(x_i),2))
delta_j = delta_x()
delta_eV = delta_j*eV
enthalpy = np.column_stack((x_m,delta_eV))
enthalpy = np.append(enthalpy, [[1,0]], axis=0)
enthalpy = np.insert(enthalpy,0,[[0,0]], axis=0)
hull = ConvexHull(enthalpy)

for k in range(len(x_i)):
    x = x_i[k]
    print('Calculating the coefficients a_j for x = {} and T={}...'.format(x,T))
    a_j = a_x()
    print('Done!')
    print('Calculating the eta roots of the {}-polynomial equation for x = {} and T={}...'.format(n,x,T))
    eta = eta_x()
    print('Done!')
    print('Calculating the probabilities x_j for x = {} and T={}...'.format(x,T))
    x_j = x_x()
    print('Done!')
    print('Calculating the Ti--Te bond length for x = {} and T={}...'.format(x,T))
    m_te_Ti[k] = m_te_x()
    print('Done!')
    print('Calculating the Te--Zr bond length for x = {} and T={}...'.format(x,T))
    te_zr_Ti[k] = te_zr_x()
    print('Done!')
    print('Calculating the Ti--Zr bond length for x = {} and T={}...'.format(x,T))
    m_zr_Ti[k] = m_zr_x()
    print('Done!')
    print('Calculating the Te--Te bond length for x = {} and T={}...'.format(x,T))
    te_te_Ti[k] = te_te_x()
    print('Done!')

###############################################################################################################

file = './gqca_V.dat'
data = np.genfromtxt(file)

file_2 = './dist_V.dat'
data_2 = np.genfromtxt(file_2)

n_j = np.int64(data[:,0]) ## Number of M atoms in the supercell
x_m = (data[:,1])
x_Zn = data[:,1]
g_j = data[:,2] ## Cluster degeneracy
e_j = (data[:,7])
m_te_j = data_2[:,4]
te_zr_j = data_2[:,5]
m_zr_j = data_2[:,6]
te_te_j = data_2[:,7]

x_i = np.linspace(0.0001,0.9999,num=2000)
t_i = np.linspace(10,800,num=1000)

#Calculating the physical properties using GQCA
m_te_V = np.zeros((len(x_i),2))
te_zr_V = np.zeros((len(x_i),2))
m_zr_V = np.zeros((len(x_i),2))
te_te_V = np.zeros((len(x_i),2))
delta_j = delta_x()
delta_eV = delta_j*eV
enthalpy = np.column_stack((x_m,delta_eV))
enthalpy = np.append(enthalpy, [[1,0]], axis=0)
enthalpy = np.insert(enthalpy,0,[[0,0]], axis=0)
hull = ConvexHull(enthalpy)

for k in range(len(x_i)):
    x = x_i[k]
    print('Calculating the coefficients a_j for x = {} and T={}...'.format(x,T))
    a_j = a_x()
    print('Done!')
    print('Calculating the eta roots of the {}-polynomial equation for x = {} and T={}...'.format(n,x,T))
    eta = eta_x()
    print('Done!')
    print('Calculating the probabilities x_j for x = {} and T={}...'.format(x,T))
    x_j = x_x()
    print('Done!')
    print('Calculating the V--Te bond length for x = {} and T={}...'.format(x,T))
    m_te_V[k] = m_te_x()
    print('Done!')
    print('Calculating the Te--Zr bond length for x = {} and T={}...'.format(x,T))
    te_zr_V[k] = te_zr_x()
    print('Done!')
    print('Calculating the V--Zr bond length for x = {} and T={}...'.format(x,T))
    m_zr_V[k] = m_zr_x()
    print('Done!')
    print('Calculating the Te--Te bond length for x = {} and T={}...'.format(x,T))
    te_te_V[k] = te_te_x()
    print('Done!')

###############################################################################################################

file = './gqca_Zn.dat'
data = np.genfromtxt(file)

file_2 = './dist_Zn.dat'
data_2 = np.genfromtxt(file_2)

n_j = np.int64(data[:,0]) ## Number of M atoms in the supercell
x_m = (data[:,1])
x_Zn = data[:,1]
g_j = data[:,2] ## Cluster degeneracy
e_j = (data[:,7])
m_te_j = data_2[:,4]
te_zr_j = data_2[:,5]
m_zr_j = data_2[:,6]
te_te_j = data_2[:,7]

x_i = np.linspace(0.0001,0.9999,num=2000)
t_i = np.linspace(10,800,num=1000)

#Calculating the physical properties using GQCA
m_te_Zn = np.zeros((len(x_i),2))
te_zr_Zn = np.zeros((len(x_i),2))
m_zr_Zn = np.zeros((len(x_i),2))
te_te_Zn = np.zeros((len(x_i),2))
delta_j = delta_x()
delta_eV = delta_j*eV
enthalpy = np.column_stack((x_m,delta_eV))
enthalpy = np.append(enthalpy, [[1,0]], axis=0)
enthalpy = np.insert(enthalpy,0,[[0,0]], axis=0)
hull = ConvexHull(enthalpy)

for k in range(len(x_i)):
    x = x_i[k]
    print('Calculating the coefficients a_j for x = {} and T={}...'.format(x,T))
    a_j = a_x()
    print('Done!')
    print('Calculating the eta roots of the {}-polynomial equation for x = {} and T={}...'.format(n,x,T))
    eta = eta_x()
    print('Done!')
    print('Calculating the probabilities x_j for x = {} and T={}...'.format(x,T))
    x_j = x_x()
    print('Done!')
    print('Calculating the Zn--Te bond length for x = {} and T={}...'.format(x,T))
    m_te_Zn[k] = m_te_x()
    print('Done!')
    print('Calculating the Te--Zr bond length for x = {} and T={}...'.format(x,T))
    te_zr_Zn[k] = te_zr_x()
    print('Done!')
    print('Calculating the Zn--Zr bond length for x = {} and T={}...'.format(x,T))
    m_zr_Zn[k] = m_zr_x()
    print('Done!')
    print('Calculating the Te--Te bond length for x = {} and T={}...'.format(x,T))
    te_te_Zn[k] = te_te_x()
    print('Done!')

###############################################################################################################

rc('font',**{'family':'Palatino','serif':['Palatino'],'size':22})
rc('text', usetex=True)

colormap = Palette(90, 2, 'coolwarm')
colorplot = colormap.get_colors()

print('Plotting bond lengths...')
plot_m_te()
plot_te_zr()
plot_m_zr()
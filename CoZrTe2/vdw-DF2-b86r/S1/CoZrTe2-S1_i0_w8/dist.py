# coding=utf-8
import numpy as np
import numpy.polynomial.polynomial as poly
import numpy.ma as ma
import scipy.constants as cst
import matplotlib.pyplot as plt
import matplotlib.ticker as mpt
from matplotlib import rc
from labellines import labelLine, labelLines
from colors import Palette
from scipy.spatial import ConvexHull


file = './dist.out'
data = np.genfromtxt(file)

bond = data[:,2]
dist = data[:,3]

for i in len(bond)
import numpy as np
import os
import glob
import re

def fermiEnergy(path, fileName = 'scf.out'):
    os.chdir(path)

    with open(fileName, 'r') as file:
        content = file.read()
        fermiLine = re.findall('the Fermi energy is .+', content)

    fermiLine = fermiLine[0].split()

    energy = float(fermiLine[-2])

    #os.chdir('..')
    return energy



def calculateDosFermi(path, fermiE,  filesExtension = '.dos'):
    os.chdir(path)

    files = glob.glob(f'*{filesExtension}')
    #print(files)

    allDOS = []
    for currentFile in files:
        print('-'*50)
        print(currentFile)
        content = np.genfromtxt(currentFile)
        numColumns = len(content[0])

        energies = content[:,0]

        try:
            fermiIndex = energies.tolist().index(fermiE)

            DOS = []
            for currentColumn in range(1, numColumns):
                column = content[:, currentColumn]
                DOS.append(column[fermiIndex])

            allDOS.append(DOS)

        except:
            energiesWithFermi = np.append(energies, fermiE)
            energiesWithFermi = np.sort(energiesWithFermi)

            fermiIndex = energiesWithFermi.tolist().index(fermiE)

            #print('-'*50)
            #print(currentFile)

            DOS = []
            for currentColumn in range(1,numColumns):

                column = content[:, currentColumn]

                #interp
                x = [energies[fermiIndex-1], energies[fermiIndex]]
                y = [column[fermiIndex-1], column[fermiIndex]]
                dosFermi = np.interp(fermiE,x,y)
                print(energies[fermiIndex-1], column[fermiIndex-1])
                print(fermiE, dosFermi)
                print(energies[fermiIndex], column[fermiIndex])

                newColumn = np.concatenate((column[:fermiIndex],[dosFermi],column[fermiIndex:]))
                DOS.append(newColumn[fermiIndex])

            allDOS.append(DOS)
    #os.chdir('..')

    return files, allDOS

if __name__ == '__main__':
    file = 'nscf.out'
    path = './dos'
    fermi = fermiEnergy(path, file)

    calculateDosFermi(path,fermi, filesExtension='.dos')

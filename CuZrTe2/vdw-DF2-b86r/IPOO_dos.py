from typing import ContextManager, Mapping
import numpy as np
import os
import glob

class dos_files:

    def __init__(self, path_dos, prefix, atoms, orbitals = {'s':1,'p':3,'d':5,'f':7}):
        self.path_dos = path_dos
        self.prefix = prefix
        self.atoms = atoms
        self.orbitals = orbitals

        os.chdir(path_dos)

        self.atom_orbt = []
        self.files = []
        self.all_sumpdos = []

        for current_atom in self.atoms: #maps through the atoms
            for current_orbital in self.orbitals: #maps through the orbitals
                current_orbital_files = glob.glob(f'{prefix}*({current_atom})*({current_orbital})*')

                num_files = len(current_orbital_files) #number of files in current orbital

                self.atom_orbt.append([f'{current_atom}_{current_orbital}',num_files])
                self.files.append(current_orbital_files)
                self.all_sumpdos.append([])

                #print('-'*50)
                #print(current_orbital, current_orbital_files)

                if num_files: #if there are files in current orbital
                    file_content = np.genfromtxt(current_orbital_files[0])
                    lenthE = len(file_content)

                    self.energy = file_content[:, 0] #file column 1

                    sumpdos = np.zeros((orbitals[current_orbital], lenthE))

                    for current_file in current_orbital_files:

                        file_content = np.genfromtxt(current_file)

                        #ldos = file_content[:, 1] #column 2

                        pdos = [] #other columns
                        for pdos_column in range(2,self.orbitals[current_orbital]+2):
                            pdos.append(file_content[:, pdos_column])

                        pdos = np.array(pdos)
                        sumpdos += pdos

                    self.all_sumpdos[-1] = sumpdos
                else:
                    self.files[-1] = f'There are no files in {current_orbital} orbital'
                    self.all_sumpdos[-1] = f'There are no files in {current_orbital} orbital'

    def return_values(self):
        return self.files, self.all_sumpdos

    def pdos_files(self):
        for current_atom_orbt in self.atom_orbt:
            current_atom_orbt_index = self.atom_orbt.index(current_atom_orbt)
            current_sumpdos = self.all_sumpdos[current_atom_orbt_index]
            name = current_atom_orbt[0]
            num_files = current_atom_orbt[1]

            if num_files:
                with open(f'{name}.dos','w') as wrt_file: #open writable file

                    wrt_file.write('#E           ')
                    for column in range(len(current_sumpdos)):
                        wrt_file.write(f'column {column}     ')
                    wrt_file.write(f'ldos')
                    wrt_file.write('\n')

                    for line in range(len(self.energy)): #maps through all values of energy
                        pdos_totalsum = 0
                        wrt_file.write(f'{self.energy[line]:.6f}     ')

                        for column in current_sumpdos: #maps trhough all columns of sumpdos
                            current_value = column[line]

                            wrt_file.write(f'{current_value:.6f}     ')
                            pdos_totalsum += current_value

                        wrt_file.write(f'{pdos_totalsum:.6f}')
                        wrt_file.write('\n')


    def pdos_graphs(self):
        pass

#test area
if __name__ == '__main__':
    path = './S0/CuZrTe2-S0_i0_w1'
    prefix = 'CuZrTe2-S0_i0_w1'
    atoms = ['Cu','Zr','Te']
    sistema = dos_files(path, prefix, atoms)
    #values = sistema.return_values()
    sistema.pdos_files() #save files

    #for arquivo in range(len(sistema.atoms)):
    #    print(f'{sistema.atoms[arquivo]} - {sistema.files[arquivo]}')

\documentclass[prb,aps,showpacs,preprintnumbers,amsmath,amssymb,twocolumn,superscriptaddress]{revtex4-2}
\usepackage[utf8]{inputenc}
\usepackage{graphicx,pbox,lineno}
\graphicspath{{./Figuras/}}
\usepackage[dvipsnames]{xcolor}
\usepackage[normalem]{ulem}
% \usepackage{subfig}
\allowdisplaybreaks
\usepackage[colorlinks, citecolor={blue!50!black}, urlcolor={blue!50!black}, linkcolor={red!50!black}]{hyperref}
\usepackage[caption=false]{subfig}

\newcommand{\note}[1]{\textcolor{blue}{#1}}
\newcommand{\comment}[1]{\textcolor{red}{(#1)}}

\begin{document}

%\linenumbers

\title{Structural, electronic, and thermodynamic properties of intercalated transition metal dichalcogenide ZrTe$_2$ from ab initio statistical thermodynamic models}
\author{P. P. Ferreira}
\email[Corresponding author: ]{pedroferreira@usp.br}
\affiliation{Computational Materials Science Group (ComputEEL/MatSci), Universidade de S\~ao Paulo, Escola de Engenharia de Lorena, DEMAR, Lorena, Brazil}
\affiliation{Institute of Theoretical and Computational Physics, Graz University of Technology, NAWI Graz, 8010 Graz, Austria}
\author{T. T. Dorini}
\affiliation{Universit\'e de Lorraine, CNRS, IJL, Nancy, France}
\author{I. Guilhon}
\affiliation{Grupo de Materiais Semicondutores e Nanotecnologia, Instituto Tecnológico de Aeronáutica, DCTA, 122228-900, São José dos Campos, Brazil}
\author{M. Marques}
\affiliation{Grupo de Materiais Semicondutores e Nanotecnologia, Instituto Tecnológico de Aeronáutica, DCTA, 122228-900, São José dos Campos, Brazil}
\author{L. K. Teles}
\affiliation{Grupo de Materiais Semicondutores e Nanotecnologia, Instituto Tecnológico de Aeronáutica, DCTA, 122228-900, São José dos Campos, Brazil}
\author{E. Gaudry}
\affiliation{Universit\'e de Lorraine, CNRS, IJL, Nancy, France}
\author{C. Heil}
\affiliation{Institute of Theoretical and Computational Physics, Graz University of Technology, NAWI Graz, 8010 Graz, Austria}
\author{L. T. F. Eleno}
\email[Corresponding author: ]{luizeleno@usp.br}
\affiliation{Computational Materials Science Group (ComputEEL/MatSci), Universidade de S\~ao Paulo, Escola de Engenharia de Lorena, DEMAR, Lorena, Brazil}

\begin{abstract}

The present work investigates the thermodynamic stability, structural, electronic, and bonding properties of ZrTe$_2$ intercalated with several transition metal species by combining statistical thermodynamic models with first-principles electronic-structure calculations. Our results show that a realistic intercalated transition metal dichalcogenide alloy grown from equilibrium processes tends to a homogenous phase, with a random, disordered dopant distribution within the van der Waals gap. This information is crucial for studying/manipulating alloy properties sensitive to the local environment and emergent long-range order phenomena, such as charge density waves. 

\end{abstract}

\date{\today}

\pacs{}

\maketitle

\section{Introduction}

Transition metal chalcogenides are a large, widely studied class of materials that exhibit many unique electronic, elastic, thermodynamic, and topological properties. Especially, transition metal dichalcogenides (TMDs) are formed by weakly bound $MX_2$ stacking-ordered layers, where $M$ is a transition metal, and $X$ is a chalcogen, either S, Se, or Te \cite{chhowalla2013, manzeli2017}. Chemically stable thin layers of TMDs can be easily obtained via graphene-like mechanical exfoliation from bulk high-quality single crystals due to their weak interlayer van der Waals interactions \cite{novoselov2005}. In fact, either bulk and thin exfoliated, atomically flat TMDs have been considered a likely avenue to success for opto-valleytronic and interband tunnel field-effect transistors with ultralow standby power consumption \cite{podzorov2004, radisavljevic2011, xiao2012, zhang2014, mak2014}, ultrasensitive photodetectors \cite{lopez2013}, straintronics devices \cite{miao2021}, charge qubit-based technologies \cite{lucatto2019}, battery cathodes \cite{whittingham2004}, energy conversion electrocatalysts \cite{voiry2016}, and solid lubricants \cite{lee2010}, to cite some few examples.

ZrTe$_2$ has drawn much research interest regarding metallic TMD phases over the last few years. High-resolution angle-resolved photoemission spectroscopy and density functional theory calculations show that ZrTe$_2$ is a semimetal composed by three Te-$p$ hole-like nondegenerate bands near the $\Gamma(A)$ point and a single Zr-$d$ electron-like pocket at the $M(L)$ \cite{kar2020}. On its topological nature, there are few contradictory studies, though. DFT calculations suggest a band inversion between Te-$p$ and Zr-$d$ states at $\Gamma$ point \cite{kar2020}, somewhat reminiscent of topological insulators driven by spin-orbit coupling. However, measured nuclear magnetic resonance shift anisotropy reveals a quasi-2-dimensional behavior connected to a topological nodal line close to the Fermi level \cite{tian2020}. We have contested those interpretations recently, one some of our calculations showed, based on the parity analysis of the irreducible representations, that a non-trivial topological type-II state is much more likely with the appropriate band tuning \cite{correa2022}. The high Dirac cones' energy level makes any possible important contribution of emergent Dirac-type quasiparticles in undoped ZrTe$_2$ quite unlikely, contrary to what has been claimed elsewhere.

Regardless of its tremendous scientific interest,
ZrTe$_2$ has also been intensively studied from a technological standpoint. Recently, ZrTe$_2$ was used in conjunction with CrTe$_2$ as a proof-of-concept spintronic device of wafer-scale van der Waals Dirac semimetal/2D ferromagnet heterostructure, with current-driven magnetization switching \cite{ou2021}. Heterostructures of ZrTe$_2$ and SrTiO$_3$ were explored as well, revealing an enormous thermoelectric power factor as high as $4\times10^5 \mu Wcm^{-1}K^{-2}$, thus paving the way for designing high-performance thermoelectric devices \cite{suen2021}. Additionally, a 60\,mm thick ZrTe$_2$ film shows features such as high two-carrier mobility as large as $1.8\,\times10^4$\,cm$^2$V$^{-1}$s$^{-1}$, a robust linear magnetoresistance of 3000\,\% at 2\,K under a perpendicular magnetic field of 9\,T, and negative magnetoresistance when subjected to magnetic fields parallel to the current direction \cite{wang2019}. ZrTe$_2$ was also used to demonstrate for the first time the usage of semimetallic monolayers as nanoscatter medium for providing feedback in an organic random laser emission without photodegradation \cite{pincheira2020}, and as nonlinear electric dipoles for second harmonic wave generation, holding the podium as the largest orientation-averaged first-hyperpolarizability reported so far \cite{da2020}. 

The pristine CdI$_2$-type structure of ZrTe$_2$ is a friendly environment for tweaking and fine-tuning the electronic ground state through intercalation of other atomic species and complexes in the region between adjacent chalcogen planes. Some of the present authors, for instance, reported Cu-intercalation induced pressure-resistant superconductivity in ZrTe$_2$ \cite{machado2017}. In particular, Ni$_{0.04}$ZrTe$_2$ shows resistivity and heat capacity anomalies that are consistent with a possible coexistence of superconductivity and charge density waves \cite{correa2022}. Notably, further scanning tunneling microscopy and spectroscopy reveal the emergence of a commensurate 2$\times$2 CDW order following an interface- and Zr intercalation-induced semiconductor-metal crossover in ultrathin ZrX$_2$ (X = Se, Te) films supporting the experimental findings by Correa et.\,al (2021) \cite{correa2022}, then raising several important, unsolved questions on the CDW mechanisms in those compounds \cite{ren2021}. Recently, a possible pressure-induced CDW transition was also considered in pure and doped ZrTe$_2$ due to a broad wave-like maximum at about 2\,GPa in thermopower measurements \cite{morozova2021}. Cr-intercalated atoms also cause a significant change of ZrTe$_2$ electronic ground state, driving the system into a strong temperature-dependent indirect-gap state, with the valence bands around $\Gamma$-point sinking below the Fermi level \cite{zhang2020}. Superconductivity is also reported, although a more robust and complete characterization is needed \cite{zhang2020}.

Therefore, ZrTe$_2$ has proven to be a fertile soil attracting research efforts worldwide with the promising to pave the road for a wide range of both technological applications and high-standard fundamental research.



\section{Methodology}

\label{sec:methods}

\subsection{Generalized Quasichemical Approximation}
\label{sec:gqca}

In the cluster expansion method within generalized quasichemical approximation \cite{sher1987}, the system is described by an ensemble of $M$ individual clusters statistically and energetically independent of the surrounding atomic configuration with $n$ sites that may be occupied by $A$ or $B$. In our case, the 8 available nonequivalent atomic sites of the 1$b$ Wychoff position can be occupied either by a transition metal specie or by a vacancy. In this way, there are $N_{\text{vac}} = (1-x)N$ vacancies and $N_{\text{tm}} = xN$ atoms from transition metal group,  being $N = N_{\text{vac}} + N_{\text{tm}}$. The clusters can be arranged then in 22 non-equivalent symmetrically classes with distinct total energies $E_j$ and degeneracies $g_j$, with $j = 0,1,\ldots,22$. The cluster configurations for the $2\times2\times2 $ octahedraly coordinated TMDs with their respective degeneracies are described in Tab.~\ref{tab:GQCA}. In each class there are $M_j$ clusters with energy $E_j$, with
\begin{align}
M = \sum_{j = 0}^{J}M_j,
\label{eq:M}
\end{align}
and by introducing the cluster fraction  probability,
\begin{align}
x_j = \dfrac{M_j}{M},
\label{eq:x_j}
\end{align}
it holds the relations
\begin{align}
	\sum_{j=0}^{J} x_j &= 1,
	\label{eq:normalization}
\end{align}
and
\begin{align}
	\sum_{j=0}^{J} n_jx_j &= nx.
	\label{eq:average}
\end{align}

\begin{table}
	\caption{The 22 symmetrically-independent cluster classes of M$_x$ZrTe$_2$ alloys for trigonal 8-atom supercells. The "M atom'' column indicates the spatial configuration of the intercalating species for each $j$ class. The site label it's found in Fig.~\ref{fig:cell}.}
	\label{tab:GQCA}
	\centering
	\begin{tabular}{lccccccc}
	    \toprule
		%
		$j$ & $n_j$ & $g_j$ & M atoms & $j$ & $n_j$ & $g_j$ & M atoms\\
		\hline
		1 & 0 & 1 & -- & 12 & 4 & 6 & 1,2,7,8\\[2mm]
		2 & 1 & 8 & 1 & 13 & 4 & 24 & 1,2,3,7\\[2mm]
		3 & 2 & 4 & 1,8 & 14 & 4 & 24 & 1,2,3,6\\[2mm]
		4 & 2 & 12 & 1,3 & 15 & 5 & 24 & 1,2,3,4,5\\[2mm]
		5 & 2 & 12 & 1,3 & 16 & 5 & 8 & 1,2,3,5,7\\[2mm]
		6 & 3 & 8 & 1,3,5 & 17 & 5 & 24 & 1,2,3,6,7\\[2mm]
		7 & 3 & 24 & 1,2,7 & 18 & 6 & 4 & 1,2,3,6,7,8\\[2mm]
		8 & 3 & 24 & 1,2,3 & 19 & 6 & 12 & 1,2,3,4,5,7\\[2mm]
		9 & 4 & 6 & 1,2,3,4 & 20 & 6 & 12 & 1,2,3,4,5,6\\[2mm]
		10 & 4 & 8 & 1,2,3,5 & 21 & 7 & 8 & 1,2,3,4,5,6,7\\[2mm]
		11 & 4 & 2 & 1,3,5,7 & 22 & 8 & 1 & 1,2,3,4,5,6,7,8\\[2mm]					
		\toprule
	\end{tabular}
\end{table}

With that, one can easily calculate the mixing internal energy as
\begin{align}
\Delta U(x,T) = M\sum_{j=0}^{J} x_j \Delta_j.
\label{eq:delta_U_2}
\end{align}
The reduced excess energies, $\Delta_j$, are defined by
\begin{align}
\Delta_j = E_j - \dfrac{n - n_j}{n}E_0 - \dfrac{n_j}{n}E_J, 
\end{align}
where $n_j$ is the total number of atoms of type B in cluster $j$.
 
Within microcanonical ensemble, the mixing entropy can be obtained from the classical Boltzmann definition
\begin{align}
	\Delta S(x,T) = k_B\ln W,
	\label{eq:delta_S}
\end{align}     
where
\begin{align}
	W = \dfrac{N!}{N_A!N_B!}\dfrac{M!}{\prod_{j=0}^{J}M_j!}\prod_{j=0}^{J}{x_{j}^{0}}^{M_j}
	\label{eq:W}
\end{align}
is the total number of possible configurations of a random, disordered alloy formed by the cluster configurations $M_0, M_1, \ldots, M_j$; $k_B$ is the Boltzmann constant; and	$x_j^0 = g_j x^{n_j}(1-x)^{n-n_j}$
is the $j$ cluster fraction for a given composition $x$ considering an ideal solid solution. 

Using Stirling's approximation, $\ln N! \approx N\ln N - N$, one can develop Eq. \ref{eq:W} until find 
\begin{align}
	\ln W = -N\left[x\ln x + (1-x)\ln(1-x)\right] \nonumber\\- M\sum_{j=0}^{J}x_j\ln\left(\dfrac{x_j}{x_j^0}\right).	
\end{align}
Hence, the mixing free energy within the generalized quasichemical approximation is written as
\begin{align}
\Delta F(x,T) =& M\sum_{j=0}^{J} x_j \Delta_j  \nonumber\\ &+ Nk_BT\left[x\ln x + (1-x)\ln(1-x)\right] \nonumber\\ &+ Mk_BT\sum_{j=0}^{J}x_j\ln\left(\dfrac{x_j}{x_j^0}\right),
\end{align} 
where the entropy term
\begin{align}
	D_{KL}(x,T) = \sum_{j=0}^{J}x_j\ln\left(\dfrac{x_j}{x_j^0}\right) 
\end{align}
is the Kullback-Leibler divergence \cite{kullback1951}, which evaluates the similarity/dissimilarity between the GQCA cluster occurrence probability, $x_j$, and the probability distribution of cluster $j$ in an ideal solid solution, $x_j^0$. Since the equilibrium state of a system maintained at constant temperature and pressure corresponds to the state of minimum Gibbs free energy, which in our case it is equal to the Helmholtz free energy, the unknown cluster probabilities $x_j$ will be those in which $\Delta F$ per cluster is minimized.


Employing the Lagrange multipliers formalism with respect to constraints (\ref{eq:normalization}) and (\ref{eq:average}), one can define the cluster occurrence probability as
\begin{align}
	x_j = \dfrac{g_j\eta^{n_j} e^{-\Delta_j/k_BT}}{\sum_{j=0}^{J}g_j\eta^{n_j} e^{-\Delta_j/k_BT}},
	\label{eq:xj}
\end{align}
where 
\begin{align}
	\eta = \dfrac{xe^{\lambda_2/k_BT}}{1-x}.
\end{align}

Therefore, for finding the $x_j\left(x,T\right)$ probabilities, one have to solve the following $8$-order ($n = 8$) polynomial equation, 
\begin{align}
	\sum_{j=0}^{J}\left(nx - n_j\right)g_je^{-\Delta_j/k_BT}\eta^{n_j} = 0,
\end{align}
in which exists just one real solution. When one has done that, one can easily estimate any alloy property, $P(x,T)$, as an average of $P_j$ values, obtained \emph{ab initio}, weighted by the probabilities $x_j$ that minimize the overall mixing free energy:
\begin{align}
P(x, T) = \sum_{j=0}^{J}x_j(x,T)P_j.
\end{align} 
As a consequence, one can evaluate the uncertainty of the theoretical estimation by the standard deviation associated with the distribution of the weighted $P_j$ values,  
\begin{align}
\Delta P(x,T) = \sqrt{\left[\sum_{j=0}^{J}x_j(x,T)P_j^2\right] - \left[\sum_{j=0}^{J}x_j(x,T)P_j\right]^2},
\end{align}
thus enabling a robust, straightforward way to define upper and lower bound limits compared with objective experimental measurements. The GQCA model is also carefully described elsewhere \cite{chen1995,teles2000,guilhon2015,guilhon2017}.

\subsection{Crystal Orbital Hamilton Populations}

\subsection{Computational methods}

\section{Results and Discussion}

\begin{figure*}[h]
	\includegraphics[width=\linewidth]{./lat-par}
	\caption{Lattice parameters}
	\label{fig:lat-par}
\end{figure*}

\begin{figure}[h]
	\includegraphics[width=\linewidth]{./stability}
	\caption{Helmholtz free energy}
	\label{fig:stability}
\end{figure}

\section{Conclusions}

\section*{Author Contributions}

PPF implemented the GQCA model, performed the DFT calculations, analyzed the results, and wrote the manuscript. IG, MM, and LKT helped with the code implementation and discussions. TTD and EG carried out the COHP calculations. CH and LTF supervised this project and helped with the discussions and writing. All co-authors revised the manuscript.

\section*{Acknowledments}

We gratefully acknowledge the financial support of the S\~ao Paulo Research Foundation (FAPESP) under Grants 2020/08258-0 and 2021/13441-1. This study was also financed by the Coordena\c c\~ao de Aperfei\c coamento de Pessoal de N\' ivel Superior (CAPES) - Brasil - Finance Code 001. The research was carried out using high-performance computing resources made available by the Superintend\^encia de Tecnologia da Informa\c c\~ao (STI), Universidade de S\~ao Paulo. The authors also acknowledge the National Laboratory for Scientific Computing (LNCC/MCTI, Brazil) for providing HPC resources of the SDumont supercomputer, which have contributed to the research results reported within this paper.


\bibliographystyle{apsrev4-1}
\bibliography{refs}


\end{document}




#!/bin/sh
for i in S*
do

cd $i

for j in CrZrTe2*
do

cd $j

cat > pdos.in << EOF
&projwfc
        prefix = '$j'
        outdir = './tmp/'
        DeltaE = 0.01 
        filpdos = 'CrZrTe2'
/
EOF

cat > pdos.job << EOF
#!/bin/sh
#SBATCH --partition=SP2
#SBATCH --ntasks=40
#SBATCH --ntasks-per-node=20
#SBATCH -J $j        # nome do job que aparece no comando squeue
#SBATCH --time=192:00:00
#SBATCH --mem-per-cpu=24042
#SBATCH --export=ALL

export OMP_NUM_THREADS=1

module purge
module load ohpc
source /opt/intel/mkl/bin/mklvars.sh intel64
export PATH=/scratch/7290265/qe-6.7.0/bin:\$PATH
cd /scratch/7290265/ppf/CrZrTe2/vdw-DF2-b86r/$i/$j
mpirun -np \$SLURM_NTASKS pw.x -nk 2 < scf.in > scf.out
mpirun -np \$SLURM_NTASKS pw.x -nk 2 < nscf.in > nscf.out
mpirun -np \$SLURM_NTASKS projwfc.x -nk 2 < pdos.in > pdos.out
EOF

cd ..

done

cd ..

done

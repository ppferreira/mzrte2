#!/bin/sh
for i in S*
do

cd $i

for j in CrZrTe2*
do

cd $j

for file in scf.out
do

efermi=`grep " the Fermi energy is" $file | gawk '{print $5}'`

done

arquivo="pdos"

cat > $arquivo.py << EOF

# coding=utf-8
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mpt
from matplotlib import rc

## defining some parameters
fermiEnergy = $efermi ## Fermi energy from self-consistent or non-self consistent calculation

## path to the band structure file
file1 = './$j.pdos_tot'

## data import from file
data1 = np.float64(np.genfromtxt(file1, usemask='True'))

## data as numpy arrays
energy = data1[:,0]-fermiEnergy
dos_tot = data1[:,1]/8

print(type(data1[:,0]))

## determining DOS at EF
print(type(fermiEnergy))
fermi_index = np.int64(np.where(np.isclose(data1[:,0],round(fermiEnergy,2))))
print(fermi_index)
diff = round(fermiEnergy,2) - fermiEnergy
if diff > 0:
    x = [data1[fermi_index-1,0][0,0],data1[fermi_index,0][0,0]]
    y = [data1[fermi_index-1,1][0,0]/8,data1[fermi_index,1][0,0]/8]
    dos_fermi = np.interp(fermiEnergy,x,y)
    print('The total DOS at the Fermi level is {}'.format(dos_fermi))
else:
    x = [data1[fermi_index,0][0,0],data1[fermi_index+1,0][0,0]]
    y = [data1[fermi_index,1][0,0]/8,data1[fermi_index+1,1][0,0]/8]
    dos_fermi = np.interp(fermiEnergy,x,y)
    print('The total DOS at the Fermi level is {}'.format(dos_fermi))

## band plot
rc('font',**{'family':'Palatino','serif':['Palatino'],'size':20})
rc('text', usetex=True)

plt.figure()
plt.xlim(-5.5, 5)
plt.ylim(0,14)
plt.xlabel('Energy\,(eV)')
plt.ylabel('DOS\,(states/eV)')
plt.axvline(x=0, color='black')
plt.text(-3.2, 13, 'N(E\$_F\$) = {}'.format(round(dos_fermi,3)), horizontalalignment='center', verticalalignment='center')

line1, = plt.plot(energy,dos_tot,  color='black', linestyle='solid', linewidth='1.8', antialiased=True, label='Total')
plt.legend(handles=[line1], loc=1, fontsize='16')


plt.savefig('pdos.png', bbox_inches='tight')

EOF

python3 $arquivo.py

cd ..

done

cd ..

done
